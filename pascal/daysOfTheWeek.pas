﻿program daysOfTheWeek;
var days: array [1..7] of string = ('Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday');
num: integer;
begin
  writeln('Input number of the day of the week');
  read(num);
  if (num < 1) or (num > 7) then
    writeln('There id no such day')7
  else
    writeln('It is ', days[num]);
end.