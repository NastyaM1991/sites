﻿var
a,b,c:integer;

function isMiddle(x,y,z:integer): Boolean;
begin
  if ((x<=y) and (x>=z)) or ((x<=z) and (x>=y)) then
    isMiddle:= true
  else  
    isMiddle:= false;  
end;  
    
begin 
  write('input a = ');
  readln(a);
  
  write('input b = ');
  readln(b);
  
  write('input c = ');
  readln(c);
  
 
  if isMiddle(a,b,c) then
    writeln('max v = ', a)
  else
  begin
    if isMiddle(b,a,c) then
      writeln('max v = ', b)
     else
     begin
       writeln('max v = ', c);
     end;
  end;
end.