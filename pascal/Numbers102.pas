﻿program Numbers_10_2;
var N10, N2, k: longint;
begin
  write('N10 = '); readln(N10);
  k := 1;
  N2 := 0;
  repeat
    N2 := N2 + (N10 mod 2) * k;
    k := k*10;
    N10 := N10 div 2;
  until (N10 = 0);
  writeln('N2 = ', N2);
end.