﻿program krat64;
var currentNumber, sum: integer;
begin
  sum := 0;
  
  repeat
    writeln('Input current number');
    read(currentNumber);
    
    if (currentNumber <> 0) and (currentNumber mod 6 = 0) and (currentNumber mod 10 = 4) then sum := sum + currentNumber;
  until currentNumber = 0;
  
  writeln(sum);
end.