﻿program oge152;
var num, count, sum: integer;
begin
  sum := 0;
  count := 0;
  repeat
    writeln('Input number');
    read(num);
    
    if (num mod 8 = 0) and (num <> 0) then
    begin
      count := count + 1;
      sum := sum + num;
    end;
  until num = 0;
  
  if count = 0 then writeln('NO')
  else writeln(sum/count:0:1);
end.