﻿program ege;
const N = 5;
var a: array[1..N] of integer;
i, k, m: integer;
begin
 
  for i := 1 to N do
  begin
    readln(a[i]);
    if a[i] mod 10 = 4 then k := k + 1;
  end;
  
  for i := 1 to N do
  begin
    if a[i] mod 10 = 4 then a[i] := k;
    writeln(a[i]);
  end;
end.