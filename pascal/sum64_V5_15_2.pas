﻿program sumOfnumbers;
var num, sum: integer;
begin
  sum := 0;
  repeat
    writeln('Введите число');
    read(num);
    
    if (num mod 6 = 0) and (num mod 10 = 4) then sum := sum + num;
  until num = 0;
  writeln(sum);
end.