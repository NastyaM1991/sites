﻿program srArefm15_2;
var sum, quantity, currentNumber: integer;
sred: real;
begin 
  sum := 0;
  repeat
    writeln('Input number');
    read(currentNumber);

    if (currentNumber mod 8 = 0) and (currentNumber <> 0) then
    begin
      quantity := quantity + 1;
      sum := sum + currentNumber;
    end;
  until currentNumber = 0;
  
  if sum = 0 then writeln('NO')
  else
  begin
    sred := sum / quantity;
    writeln(sred);
  end;
end.