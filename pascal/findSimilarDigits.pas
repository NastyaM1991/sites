﻿program SimilarDigits;
var num, d1, d2: integer;
similar: boolean;
begin
  writeln('Введите число');
  read(num);
  
  d2 := -1; {записываем невозможное значение цифры}
  similar := false;
  while num <> 0 do
  begin
    d1 := num mod 10;
    if d1 = d2 then
    begin
      similar := true;
      break;
    end
    else
    begin
      d2 := d1;
    end;
    num := num div 10;
  end;
  
  if similar = true then
    writeln('Ответ: да')
  else 
    writeln('Ответ: нет');
end.