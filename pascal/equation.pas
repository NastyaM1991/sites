﻿program equation;
var R, R1, t:real;
var f, d: integer;
begin
  writeln('Input t, f, d');
  read(t, f, d);
  R := power(t, 3)/4 + sqr(f) - 1/2;
  R1 := (cos(1 + d))/(1 + sin(sqrt(d+2)));
  writeln('R = ', R, ' R1 = ', R1);
end.
