﻿program oge152;
var amount, num, min, i: integer;
begin
  writeln('Input amount of numbers');
  read(amount);
  
  min:= 30000;
  for i := 1 to amount do
  begin
    writeln('Input number');
    read(num);
    
    if (num mod 10 = 4) and (num < min) then min := num;
  end;
  writeln(min);
end.