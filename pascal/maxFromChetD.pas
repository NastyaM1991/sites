﻿program D;
var num, max: integer;
begin
  max := 0;
  
  repeat
    writeln('Input number');
    read(num);
    
    if (num mod 2 = 0) and (num <> 0) and (num > max) then
    begin
      max := num;
    end;
  until num = 0;
  
  writeln(max);
end.