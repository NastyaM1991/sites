﻿program MaxDigit;
var num, d, max: integer;
begin
  writeln('Введите число');
  read(num);
  
  max := 0;
  while num <> 0 do
  begin
    d := num mod 10;
    if d > max then max := d;
    num := num div 10;
  end;
  
  writeln('Наибольшая цифра: ', max);
end.