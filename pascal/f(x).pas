﻿program functionWithIf;
var x, F: real;

begin
  writeln('Input x');
  read(x);
  
  if (x >= 0) and (x <= 3) then
    F := sqr(x)
  else
    F := 5;
  writeln('F(x) = ', F);
    
end.