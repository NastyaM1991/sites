﻿program minus;
var first, second, dif: real;

begin
  writeln('Input first and second numbers');
  read(first, second);
  
  dif := first - second;
  if dif < 0 then
    writeln('dif is negative. Dif = ', dif)
  else 
    begin
      if dif > 0 then
        writeln('dif is positive. Dif = ', dif)
      else  
        writeln('Dif = ', dif)
    end
end.