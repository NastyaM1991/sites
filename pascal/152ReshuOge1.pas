﻿program oge152;
var i, num, v, min, max, count: integer;
begin
  writeln('Input number of cars');
  read(num);
  
  min := 300;
  max := 0;
  count := 0;
  
  for i := 1 to num do
  begin
    writeln('Input velocity of the car');
    read(v);
    if v < min then min := v;
    if v > max then max := v;
    if v <= 30 then count := count + 1;
  end;
  writeln(max-min);
  writeln(count);
end.