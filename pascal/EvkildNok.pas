﻿program Evklid;
var M, N, M1, N1: integer;
NOK: real;

begin
  writeln('Введите M и N');
  read(M, N);
  
  M1 := M;
  N1 := N;
  
  while M <> N do
    begin
      if M > N then
        M := M - N
      else
        N := N - M;
    end;
  NOK := N1*M1/M;
  writeln('НОК = ', NOK);
end.