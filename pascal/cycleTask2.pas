﻿program sumFromTo;
var R, G, SUMMA, i: double;
begin
  writeln('Введите R и G');
  read(R, G);
  
  i := R;
  SUMMA := 0;
  
  while i <= G do
  begin
    SUMMA := SUMMA + i;
    i := i + 0.1
  end;
  writeln('На отрезке от ', R,' до ',G,' сумма = ',SUMMA);
end.