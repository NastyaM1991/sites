﻿program max3V5;
var i, quantity, max, currentNumber: integer;
begin
  writeln('Input quantity');
  read(quantity);
  
  max := 0;
  for i := 1 to quantity do
  begin
    writeln('Input current number');
    read(currentNumber);
    
    if currentNumber mod 10 = 3 then
    begin
      if currentNumber > max then max := currentNumber;  
    end;
  end;
  writeln(max);
end.