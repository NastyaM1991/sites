﻿program digit1;
var num, d, quantity: integer;
begin
  writeln('Введите число');
  read(num);
  
  quantity := 0;
  while num <> 0 do
  begin
    d := num mod 10;
    if d = 1 then quantity := quantity + 1;
    num := num div 10;
  end;
  
  writeln('Единиц: ', quantity);
end.