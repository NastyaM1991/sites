﻿program digits4;
var num,e,d,c,t,a: integer;
begin
  writeln('Input number');
  read(num);
  
  writeln('Input a');
  read(a);
  
  e := num mod 10;
  d := num mod 100 div 10;
  c := num mod 1000 div 100;
  t := num div 1000;
  
  if(t+c = d+e) then
    writeln('t+c = d+e');
  if((t+c+d+e) mod 3 = 0) then
    writeln('sum % 3 = 0');
  if((t*c*d*e) mod 4 = 0) then
    writeln('mul % 4 = 0');
  if((t*c*d*e) mod a = 0) then
    writeln('mul % a = 0');
end.