﻿program ege;
const N = 2019;
var a: array[1..N] of integer;
i, m1, m2: integer;
begin
  m1 := 15001;
  m2 := 0;
  for i := 1 to N do
  begin
    readln(a[i]);
    if (a[i] < m1) and (a[i] mod 2 = 0) then m1 := a[i];
    if (a[i] > m2) and (m1 mod 2 = 0) then m2 := a[i];
  end;
  
  if m1 = 15001 then m1 := 0;

  for i := 1 to N do
  begin
    if (a[i] mod 2 <> 0) and (a[i] > m1) and (a[i] < m2) then a[i] := a[i] - m1;
    writeln(a[i]);
  end;
  
end.