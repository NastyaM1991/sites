﻿program tree;
var f, i, j: integer;
begin
  writeln('Введите количество ярусов');
  read(f);
  
  if f < 1 then {иначе ничего не напечатается}
    writeln('Ёлочка не получится')
  else
  begin
    if f > 50 then {чтобы гарантированно влезало в одну строку}
      writeln('Ёлочка не поместится')
    else
    begin
      for i:=1 to f do
      begin
        for j:=1 to f-i do
        begin
          write(' ');
        end; 
        for j:=1 to 2*i-1 do
        begin
          write('*');
        end;
        writeln();
      end;
    end;
  end;
end.