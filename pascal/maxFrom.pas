﻿program maxFrom3;
var aA, aB, aV: integer;
begin
  writeln('Input how old are Anton, Boris and Victor');
  read(aA, aB, aV);
  
  if (aA > aB) and (aA > aV) then
    writeln('Anton is the oldest');
  if (aB > aA) and (aB > aV) then
    writeln('Boris is the oldest');
  if (aV > aB) and (aV > aA) then
    writeln('Victor is the oldest');
  if (aA = aB) and (aA = aV) then
    writeln('All are the same age');
  if (aA = aB) and (aA > aV) then
    writeln('Anton and Boris are older than Victor');
  if (aB = aV) and (aB > aA) then
    writeln('Boris and Victor are older than Anton');
  if (aA = aV) and (aA > aB) then
    writeln('Anton and Victor are older than Boris');
end.