﻿program maxFrom4;
var mas: array [0..3] of integer;
i, max: integer;
begin
  writeln('Input 4 numbers');
  
  for i := 0 to 3 do
    read(mas[i]);
  max := mas[0];
  
  for i := 0 to 3 do
    if mas[i] > max then
      max := mas[i];
  writeln('max = ', max);
end.