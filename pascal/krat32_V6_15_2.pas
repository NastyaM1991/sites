﻿program krat32;
var i, quantity, quantityOfnumber, currentNumber: integer;
begin
  writeln('Input quantity');
  read(quantity);
  
  quantityOfnumber := 0;
  for i := 1 to quantity do
  begin
    writeln('Input current number');
    read(currentNumber);
    
    if (currentNumber mod 3 = 0) and (currentNumber mod 10 = 2) then quantityOfnumber := quantityOfnumber + 1;
  end;
  writeln(quantityOfnumber);
end.