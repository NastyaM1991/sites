﻿program Square;
const dopustDif = 1.0E-6; {== 0.000001}
var a1, b1, c1, a2, b2, c2: real;

function findS(a, b, c: real) : real;
var p, S: real;
begin
  p := (a + b + c)/2;
  S := sqrt(p*(p-a)*(p-b)*(p-c));
  findS := S;
end;

begin 
  writeln('Input a, b, c, a1, b1, c1');
  read(a1, b1, c1, a2, b2, c2);
  
  if abs(findS(a1, b1, c1) - findS(a2, b2, c2)) <= dopustDif then
    writeln('S1 = S2')
  else
    writeln('S1 != S2');
end.