var score = 0;
var price = 10;
var scoreValue = 1;

var scoreField = document.getElementById("scoreField");
var priceField = document.getElementById("priceField");
var scoreValueField = document.getElementById("scoreValueField");

function UpdateScore(){
    score+=scoreValue;
    scoreField.innerHTML = score;
}

function BuyScoreValue(){
    if(score>=price){
        score -= price;
        scoreValue +=2;
        price *= 2;

        priceField.innerHTML = price;
        scoreValueField.innerHTML = scoreValue;
        scoreField.innerHTML = score;
    }
    else{
        alert("Not enough money");
    }
}