let nameInputField = document.getElementById("nameInputField");
let phoneInputField = document.getElementById("phoneInputField");
let descriptionInputField = document.getElementById("descriptionInputField");
let abonentsListOutputField = document.getElementById("abonentsListOutputField");

let abonentsList = [];

function RenderList()
{
    let htmlCode = "";

    for(let i = 0; i < abonentsList.length; i++)
    {
        htmlCode += "<p>" + abonentsList[i].Name + " " + abonentsList[i].Phone + " " + abonentsList[i].Description + "</p>";
    }

    abonentsListOutputField.innerHTML = htmlCode;
}

function AddAbonent()
{
    let abonent = {
        Name: nameInputField.value,
        Phone: phoneInputField.value,
        Description: descriptionInputField.value
    };

    abonentsList.push(abonent);

    nameInputField.value = "";
    phoneInputField.value = "";
    descriptionInputField.value = "";

    RenderList();
}

