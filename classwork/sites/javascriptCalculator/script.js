let generalInputField = document.getElementById("generalInputField");

let action;
let firstNum;
let secondNum;
let resultNum;

//numbers

function Button1OnClick()
{
    generalInputField.value+="1";
}

function Button2OnClick()
{
    generalInputField.value+="2";
}

function Button3OnClick()
{
    generalInputField.value+="3";
}

function Button4OnClick()
{
    generalInputField.value+="4";
}

function Button5OnClick()
{
    generalInputField.value+="5";
}

function Button6OnClick()
{
    generalInputField.value+="6";
}

function Button7OnClick()
{
    generalInputField.value+="7";
}

function Button8OnClick()
{
    generalInputField.value+="8";
}

function Button9OnClick()
{
    generalInputField.value+="9";
}

function Button0OnClick()
{
    generalInputField.value+="0";
}

function ButtonPointOnClick()
{
    generalInputField.value+=".";
}

function ButtonCOnClick()
{
    action = "";
    firstNum = "";
    secondNum = "";
    resultNum = "";
    generalInputField.value = "";
}

function ButtonCEOnClick()
{
    secondNum = "";
    generalInputField.value = "";
}

//actions

function ButtonOppositeOnClick()
{
    generalInputField.value = -generalInputField.value;
}

function ButtonDeleteOnClick()
{
    let val = generalInputField.value;
    val = val.slice(0,val.length-1);
    generalInputField.value=val;
}

function ButtonPlusOnClick()
{
    action="plus";
    firstNum = parseFloat(generalInputField.value);
    generalInputField.value = "";
}

function ButtonMinusOnClick()
{
    action="minus";
    firstNum = parseFloat(generalInputField.value);
    generalInputField.value = "";
}

function ButtonMulOnClick()
{
    action="mul";
    firstNum = parseFloat(generalInputField.value);
    generalInputField.value = "";
}

function ButtonDivideOnClick()
{
    action="divide";
    firstNum = parseFloat(generalInputField.value);
    generalInputField.value = "";
}



//count

function ButtonCalculateOnClick()
{
    secondNum = parseFloat(generalInputField.value);
   
    switch(action)
    {
        case "plus":
            resultNum = firstNum+secondNum;
            break;
        case "minus":
            resultNum = firstNum-secondNum;
            break;
        case "divide":
            if(secondNum == 0)
            {
                resultNum = 0;
                alert("You can't divide by 0");
                break;
            }
            else
            {
                resultNum = firstNum/secondNum;
                break;
            }
        case "mul":
            resultNum = firstNum*secondNum;
            break;
    }

    generalInputField.value=resultNum;
}