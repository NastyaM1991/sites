let playArea = document.getElementById("playArea");

let rocket1X, rocket1Y;
let rocket2X, rocket2Y;
let rocketH, rocketW;
let rocketDy;

let ballX, ballY;
let ballWH;
let ballDx, ballDy;

let scoreLeft = 0, scoreRight = 0;

let keysPressed;

let context;

let KeyUp1 = "w", KeyDown1 = "s";
let KeyUp2 = "ArrowUp", KeyDown2 = "ArrowDown";

function KeyDownHandler(e)
{
    if(e.key == KeyUp1 || e.key == KeyDown1 || e.key == KeyUp2 || e.key == KeyDown2)
    {
        keysPressed[e.key] = true;
    }
}

function KeyUpHandler(e)
{
    if(e.key == KeyUp1 || e.key == KeyDown1 || e.key == KeyUp2 || e.key == KeyDown2)
    {
        keysPressed[e.key] = false;
    }
}

function InitialValues()
{
    context = playArea.getContext("2d");
 
    scaleFactor = Math.sqrt(Math.pow(playArea.width, 2) + Math.pow(playArea.height, 2));
    
    rocketH = Math.floor(scaleFactor / 5);
    rocketW = Math.floor(rocketH / 8);

    rocket1X = 0;
    rocket1Y = Math.floor(playArea.height/2 - rocketH/2);

    rocket2X = playArea.width - rocketW;
    rocket2Y = rocket1Y;

    rocketDy = Math.floor(rocketH / 20);

    ballWH = rocketW;
    ballX = Math.floor(playArea.width/2 - ballWH/2);
    ballY = Math.floor(playArea.height/2 - ballWH/2);

    ballDx = 2;
    ballDy = 2;

    keysPressed = [];

    document.addEventListener("keydown", KeyDownHandler, false);
    document.addEventListener("keyup", KeyUpHandler, false);
}

function RenderObjects()
{
    context.clearRect(0, 0, playArea.width, playArea.height);

    context.fillStyle = "white";
    context.fillRect(rocket1X, rocket1Y, rocketW, rocketH);
    context.fillRect(rocket2X, rocket2Y, rocketW, rocketH);
    context.fillRect(ballX, ballY, ballWH, ballWH);

    context.textAlign = "center";
    context.font = "48px arial";
    context.fillText(scoreLeft + ":" + scoreRight, playArea.width/2, playArea.height/2);
}

function CalculatePhysics()
{
    if(keysPressed[KeyUp1] == true)
    {
        rocket1Y -= rocketDy;
        if(rocket1Y < 0)
        {
            rocket1Y = 0;
        }
    }
    if(keysPressed[KeyDown1] == true)
    {
        rocket1Y += rocketDy;
        if(rocket1Y + rocketH > playArea.height)
        {
            rocket1Y = playArea.height - rocketH;
        }
    }

    if(keysPressed[KeyUp2] == true)
    {
        rocket2Y -= rocketDy;
        if(rocket2Y < 0)
        {
            rocket2Y = 0;
        }
    }
    if(keysPressed[KeyDown2] == true)
    {
        rocket2Y += rocketDy;
        if(rocket2Y + rocketH > playArea.height)
        {
            rocket2Y = playArea.height - rocketH;
        }
    }


    ballX += ballDx;
    ballY += ballDy;

    if(ballY + ballWH >= playArea.height || ballY <= 0)
    {
        ballDy = -ballDy;
    }
    if(ballX + ballWH >= playArea.width)
    {
        scoreLeft++;
        ballDx = -ballDx;
    }
    if(ballX <= 0)
    {
        scoreRight++;
        ballDx = -ballDx;
    }

    if(ballX >= rocket1X && ballX <= + rocket1X + rocketW && ballY >= rocket1Y - ballWH/2 && ballY <= rocket1Y + rocketH + ballWH/2)
    {
        ballDx = -ballDx;
        ballX = rocket1X + rocketW + 1;
    }
    if(ballX + ballWH >= rocket2X && ballX <= rocket2X + rocketW && ballY >= rocket2Y - ballWH/2 && ballY <= rocket2Y + rocketH + ballWH/2)
    {
        ballDx = -ballDx;
        ballX = rocket2X - rocketW - 1;
    }
}

function UpdateGame()
{
    CalculatePhysics();
    RenderObjects();
}

InitialValues();
setInterval(UpdateGame, 10);