let DifSelectField = document.getElementById("DifSelectField");

let CustomDifContainer = document.getElementById("CustomDifContainer");
let CustomRightSideInputField = document.getElementById("CustomRightSideInputField");
let CustomTriesCountInputField = document.getElementById("CustomTriesCountInputField");

let UserNumberInputField = document.getElementById("UserNumberInputField");
let CompAnswerOutputField = document.getElementById("CompAnswerOutputField");

let TriesCountOutputField = document.getElementById("TriesCountOutputField");

let ButtonCheck = document.getElementById("ButtonCheck");

let LeftChangeSide = document.getElementById("LeftChangeSide");
let RightChangeSide = document.getElementById("RightChangeSide");

let triesCount;
let triesCountForMark;
let rightSide;
let leftSide;
let userNumber;
let compNumber;
let isWin;

function onDifChange()
{
    let difLevel = DifSelectField.options[DifSelectField.selectedIndex].text;
    
    if(difLevel == "CUSTOM")
    {
        CustomDifContainer.classList.remove("hidden-div");
    }
    else
    {
        CustomDifContainer.classList.add("hidden-div");
    }
}

function OutputInformation()
{
    LeftChangeSide.innerHTML = "From: " + leftSide;
    RightChangeSide.innerHTML = "To: " + rightSide;
    TriesCountOutputField.innerHTML = "count left tries: " + triesCount;
}

function GameStart()
{
    isWin = false;
    ButtonStart.disabled = true;
    ButtonCheck.disabled = false;
    let difLevel = DifSelectField.options[DifSelectField.selectedIndex].text;

    if(difLevel == "EASY")
    {
        triesCount = 50;
        rightSide = 100;
    }
    else if(difLevel == "MEDIUM")
    {
        triesCount = 25;
        rightSide = 300;
    }
    else if(difLevel == "HARD")
    {
        triesCount = 10;
        rightSide = 500;
    }
    else if(difLevel == "CUSTOM")
    {
        triesCount = parseInt(CustomTriesCountInputField.value);
        rightSide = parseInt(CustomRightSideInputField.value);
    }
    triesCountForMark = triesCount;
    leftSide = 1;
    
    compNumber = Math.round(Math.random() * rightSide) + 1;

    OutputInformation();
    alert("I guessed the number from 1 to " + rightSide);
}

function Win()
{
    CustomRightSideInputField.value = "";
    CustomTriesCountInputField.value = "";
    ButtonStart.disabled = false;
    ButtonCheck.disabled = true;
    CompAnswerOutputField.innerHTML = "!!!YOU WIN!!!";   

    isWin = true;

    let successPercent = (triesCountForMark - triesCount)*100/triesCountForMark;
    if(successPercent <= 33)
    {
        alert("Your level - Expert");
    }
    else if(successPercent > 33 && successPercent <= 66)
    {
        alert("Your level - Amateur");
    }
    else if(successPercent > 66)
    {
        alert("Your level - Beginner");
    }
}

function Lose()
{
    CustomRightSideInputField.value = "";
    CustomTriesCountInputField.value = "";
    ButtonCheck.disabled = true;
    ButtonStart.disabled = false;
    alert("YOU LOSE");
}

function CheckNumber()
{
    userNumber = parseInt(UserNumberInputField.value);
    triesCount--;

    if(userNumber == 0)
    {
        CompAnswerOutputField.innerHTML = "Input this number: " + compNumber;
    }
    else if(userNumber > compNumber)
    {
        CompAnswerOutputField.innerHTML = "Input smaller number";
        rightSide = userNumber;
    }
    else if(userNumber < compNumber)
    {
        CompAnswerOutputField.innerHTML = "Input bigger number";
        leftSide = userNumber;
    }
    else{
        Win();
    }

    if(triesCount == 0 && isWin == false)
    {
        Lose();
    }

    OutputInformation();
    UserNumberInputField.value = "";
}