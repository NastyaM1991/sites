let squareDiv = document.getElementById("squareDiv");
const step = 10;

function GetRandomColor()
{
    let color = Math.random();
    color = Math.round(color*256);
    return color;
}

function MakeDivRandomColor()
{
    let r = GetRandomColor();
    let g = GetRandomColor();
    let b = GetRandomColor();

    let color = "rgb("+r+", "+g+", "+b+")";
    squareDiv.style.backgroundColor = color;
}

function Button()
{
setInterval(MakeDivRandomColor, 100);
}

function Left()
{
    //let CurrentLeftValue = parseInt(squareDiv.style.left) || 0;
    //squareDiv.style.left = CurrentLeftValue - 10 + "px";
    squareDiv.style.left = (parseInt(squareDiv.style.left || getComputedStyle(squareDiv)['left']) - step) + 'px';
}

function Right()
{
    //let CurrentLeftValue = parseInt(squareDiv.style.left) || 0;
    //squareDiv.style.left = CurrentLeftValue + 10 + "px";
    squareDiv.style.left = (parseInt(squareDiv.style.left || getComputedStyle(squareDiv)['left']) + step) + 'px';
}

function Up()
{
    //let CurrentTopValue = parseInt(squareDiv.style.top) || 0;
    //squareDiv.style.top = CurrentTopValue - 10 + "px";
    squareDiv.style.top = (parseInt(squareDiv.style.top || getComputedStyle(squareDiv)['top']) - step) + 'px';
}

function Down()
{
    //let CurrentTopValue = parseInt(squareDiv.style.top) || 0;
    //squareDiv.style.top = CurrentTopValue + 10 + "px";
    squareDiv.style.top = (parseInt(squareDiv.style.top || getComputedStyle(squareDiv)['top']) + step) + 'px';
}