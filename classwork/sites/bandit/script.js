let firstNumOutField = document.getElementById("firstNumOutField");
let secondNumOutField = document.getElementById("secondNumOutField");
let thirdNumOutField = document.getElementById("thirdNumOutField");

let resultOutputField = document.getElementById("resultOutputField");
let resultAfterSpinOutputField = document.getElementById("resultAfterSpinOutputField");

let money = -1;

function RandomNumber(rightSide)
{
    return Math.round(Math.random()*rightSide);
}

function Reset()
{
    money = -1;
    resultOutputField.innerHTML = "Your money:";
    resultAfterSpinOutputField.innerHTML = "Result:";
    firstNumOutField.innerHTML = "";
    secondNumOutField.innerHTML = "";
    thirdNumOutField.innerHTML = "";
}

function Roll()
{
    if(money == -1)
    {
        let inStr = prompt("How much money do you have?");
        if(inStr == "")
        {
            return;
        }
        money = parseInt(inStr);
    }
    if(money >= 10)
    {
        let a = RandomNumber(10);
        let b = RandomNumber(10);
        let c = RandomNumber(10);

        firstNumOutField.innerHTML = a;
        secondNumOutField.innerHTML = b;
        thirdNumOutField.innerHTML = c;

        if(a == 7 && b == 7 && c == 7)
        {
            money += 100;
            resultAfterSpinOutputField.innerHTML = "Great +100";
            //alert("+100");
        }
        else if(a == b && a == c)
        {
            money +=30;
            resultAfterSpinOutputField.innerHTML = "Very well +30";
            //alert("+30");
        }
        else if(a == b || a == c || b == c)
        {
            money +=10;
            resultAfterSpinOutputField.innerHTML = "Not bad +10";
            //alert("+10");
        }
        else
        {
            money -= 10;
            resultAfterSpinOutputField.innerHTML = "Better luck next time -10";
            //alert("-10");
        }
        resultOutputField.innerHTML = "Your money: " + money;
    }
    else
    {
        alert("Not enough money to play");
    }
}