let sumInputField = document.getElementById("sumInputField");
let percentInputField = document.getElementById("percentInputField");
let timeInputField = document.getElementById("timeInputField");
let paymentTypeInputField = document.getElementById("paymentTypeInputField");

let allPaymentsOutputField = document.getElementById("allPaymentsOutputField");
let overpaymentOutputField = document.getElementById("overpaymentOutputField");

let monthlyPayment;

function Calculate()
{
    let payment = 0;
    if(paymentTypeInputField.options[paymentTypeInputField.selectedIndex].value == "annuity")
    {
        let percentsInMonth = percentInputField.value/100/12;
        monthlyPayment = sumInputField.value * (percentsInMonth + (percentsInMonth/((Math.pow(1+percentsInMonth,timeInputField.value))-1)));       
        payment = (monthlyPayment*timeInputField.value).toFixed(2);
    }
    else
    {
        let sum = sumInputField.value;
        let time = timeInputField.value;
        
        for(let i = 0; i < timeInputField.value; i++)
        {
            monthlyPayment = (sumInputField.value/timeInputField.value) + ((sum * percentInputField.value)/(100*12));
            sum -= sumInputField.value/timeInputField.value;
            time -= 1;
            payment += monthlyPayment;
        } 
    }
    allPaymentsOutputField.innerHTML ="Всего выплат: " + payment;
    overpaymentOutputField.innerHTML ="Переплата: " + (payment - sumInputField.value);
}