let tableFilmField = document.getElementById("tableFilmField");

let FilmNameInputField = document.getElementById("FilmNameInputField");
let DateOfWatchingInputField = document.getElementById("DateOfWatchingInputField");
let ImpressionInputField = document.getElementById("ImpressionInputField");

let FilmsList = [];

function RenderList()
{
    let htmlCode = "<table border = '1' >";

    htmlCode += "<tr>";
    htmlCode += "<th>Название фильма</th>";
    htmlCode += "<th>Дата просмотра</th>";
    htmlCode += "<th>Впечатление</th>";
    htmlCode += "</tr>";

    for(let i = 0; i < FilmsList.length; i++)
    {
        htmlCode += "<tr>";
        htmlCode += "<td>" + FilmsList[i].FilmName + "</td>";
        htmlCode += "<td>" + FilmsList[i].Date + "</td>";
        htmlCode += "<td>" + FilmsList[i].Impression + "</td>";
        htmlCode += "</tr>";
    }

    htmlCode += "</table>";

    tableFilmField.innerHTML = htmlCode;
}

function AddFilm()
{
    let film = {
        FilmName: FilmNameInputField.value,
        Date: DateOfWatchingInputField.value,
        Impression: ImpressionInputField.value
    };

    FilmsList.push(film);

    FilmNameInputField.value = "";
    DateOfWatchingInputField.value = "";
    ImpressionInputField.value = "";

    RenderList();
}