let wordInputField = document.getElementById("wordInputField");
let wordOutputField = document.getElementById("wordOutputField");
let letterInputField = document.getElementById("letterInputField");
let resetButton = document.getElementById("resetButton");

let hiddenDiv = document.getElementById("hiddenDiv");

let lengthOfWord;
let userWord;
let word = [];
let guessingLetters= [];

hiddenDiv.classList.remove("hidden-div");

function WriteGuessingWord()
{
    /*wordOutputField.innerHTML = "";
    for(let i = 0; i < lengthOfWord; i++)
    {
        wordOutputField.innerHTML += guessingLetters[i];
    }*/
    wordOutputField.innerHTML = guessingLetters.join("");
}

function MakeWord()
{
    userWord = wordInputField.value;
    lengthOfWord = wordInputField.value.length;
    for(let i = 0; i < lengthOfWord; i++)
    {
        word[i] = userWord.charAt(i);
        guessingLetters[i] = '*';
    }

    WriteGuessingWord();

    hiddenDiv.classList.add("hidden-div");
}

function Reset()
{
    word = [];
    guessingLetters = [];

    wordInputField.value = "";
    wordOutputField.innerHTML = "";
    letterInputField.value = "";
    
    hiddenDiv.classList.remove("hidden-div");
    resetButton.classList.add("hidden-div");
}

function CheckLetter()
{
    for(let i = 0; i < lengthOfWord; i++)
    {
        if(letterInputField.value == word[i])
        {
            guessingLetters[i] =  letterInputField.value;
        }
        WriteGuessingWord();
    }
    letterInputField.value = "";
   
    if(guessingLetters.join("") == word.join(""))
    {
        alert("ПОБЕДА!!!");
        resetButton.classList.remove("hidden-div");
    }
}